CONTENTS OF THIS FILE
---------------------

 * About work offline
 * Requirements
 * Installation
 * Configuration

ABOUT WORK OFFLINE
---------------------

Work offline module is used to disable cache from browser when enable work offline in browser.

REQUIREMENTS
------------

This module doesn't require the help of any other modules.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module.

CONFIGURATION
-------------
Nothing is configuration only enable and it's working.
